# Ajouter des applications
*(Tutoriel réalisé par [Isabelle Dutailly](http://dutailly.net/framakey-2-une-nouvelle-gestion-des-applications-portables))*

La Framakey 2 a modifié la gestion des applications par rapport à la version antérieure.
C’est un changement significatif et un peu désorientant de prime abord. 
Jusqu’à présent, quand on téléchargeait le pack Windows, on avait aussi 
tout un ensemble d’applications : bureautique, internet, multimédia, etc., 
ce qui avait l’avantage de tout installer en une fois, 
l’inconvénient étant d’une part un téléchargement et une installation 
pouvant prendre énormément de temps d’autre part.

## Ouverture de la Framakey : un nombre d’applications limité à l’essentiel

Une fois téléchargé et installé le pack Windows base propose un minimum d’applications.

![](images/FK2_tuto_pt_contenu-framakey-2.png)

Pas de panique, c’est tout à fait normal, vous avez bien fait un 
téléchargement complet et tout s’est installé normalement.
L’écran d’accueil vous propose une seule icône **Gérez vos applications**. 
En cliquant dessus, on va pouvoir installer d’autre logiciels pour 
personnaliser notre clé.

![](images/FK2_tuto_pt_accueil-framakey-2.png)

## Ajout d’applications

Dans la fenêtre qui s’ouvre, la Framakey vous propose une liste d’applications,
il suffit de cocher les cases, en piochant ou non dans les catégories.

![](images/FK2_tuto_pt_choix-appli.png)

Quand vous avez fait votre marché, cliquez sur le bouton Appliquer, 
la Framakey lance le processus de téléchargement-installation après avoir 
au préalable vérifié la capacité de votre clé. 
Si vous avez été trop gourmand, un message vous le signale, à vous de 
corriger le tir en décochant des applications dans la liste.
Cette partie peut prendre du temps en fonction du nombre d’applications 
à télécharger, de la qualité de votre connexion et, surtout, de la 
rapidité de votre clé.

![](images/FK2_tuto_pt_telechargement-appli.png)

Les applications se rangeront automatiquement dans leurs onglets respectifs : 
Framafox dans le panneau Internet, Libre-Office dans bureautique, etc.
Il est nettement conseillé de choisir l’application FramaKioskTune si 
vous voulez personnaliser votre clé, notamment en y ajoutant des 
logiciels que Framasoft ne propose pas ou plus.

## Ajout d’autres applications et personnalisation

Téléchargez et décompressez la ou les applications de votre choix dans 
dossier Apps de votre Framakey. Dans cet exemple, il s’agit de Calibre, 
logiciel de gestion et de conversion de livres numériques et de 
Gimp spécialisé dans le traitement d’images.

![](images/FK2_tuto_pt_applis.png)

Quand vous cliquez, dans l’onglet **Préférences** sur **Mettez à jour les menus**, 
les deux logiciels que vous avez ajoutés apparaîtront dans l’onglet **Autre**.

![](images/FK2_tuto_pt_onglet-preferences-framakey.png)

Cliquer sur **Mettez à jour les menus** pour que vos applications 
apparaissent dans le panneau **Autre** du kiosque.
Quitte à avoir une clé personnalisée autant que ça soit franchement du sur-mesure.
Rendez-vous, toujours dans le panneau **Préférences** sur l’icône **Personnalisation**. 
Une nouvelle fenêtre apparaît. 
Cliquez sur le nom de l’application portable que vous voulez personnaliser, 
par exemple celle d’un logiciel ajouté par vous comme ici Calibre. 
Remplissez ou modifiez les champs **Titre** et **Description** si nécessaire. 
Cliquez sur **Appliquer** pour valider les informations.

![](images/FK2_tuto_pt_perso-calibre-framakey.png)

Pour ranger l’application dans un autre onglet : glissez-là au bon endroit, 
cliquer aussi sur **Appliquer**. Une fois tous ces petits travaux faits, 
retourner sur les **Préférences** du kiosque et **Mettez à jour les menus**. 
Votre application sera dans le nouvel onglet.

![](images/FK2_tuto_pt_bureautique-framakey-2.png)

Vous pourrez refaire ces opérations quand vous voulez pour moduler 
la clé à votre gré et tester diverses applications. 
