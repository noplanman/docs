Getting started in diaspora\*
=============================

Part 7 – Finishing up
---------------------

We have now covered most of the main features on diaspora\*. The last
part of this tutorial series deals with the last few bits and pieces
you’ll need to know about.

### Connecting your diaspora\* seed to other services

You can connect your diaspora\* seed to several other services:
Facebook, Twitter, Tumblr and WordPress. Of course, to do this, you’ll
have to have an account on that service!

Before starting this, make sure you have your account information for
that service to hand, because you might need it to authorise your
connection with that other service in order for it to work.

To connect to another service, click Connect services in the sidebar of
the stream, and then click the relevant icon. If you’re already logged
in to your account on that service in the same browser, it should
recognise you automatically. If you’re not currently logged in to that
service, it’ll ask you to log in now. Once you’re logged in, it will ask
for authorisation to publish to your account in that service. Accept
this, and you’re ready to post directly from your diaspora\* seed to
that service!

When you next click in the publisher, you’ll see the icon for that
service under the publisher. Click this icon to activate it, and your
post will also appear in your stream on that service. Pretty nifty, eh?
See [Part 5](5-sharing.html) for more information on posting
to connected services.

### Account management

The final thing we need to look at to get you started is the user
settings page. To reach it, get to you user menu and, from the drop-down
menu, choose Settings. Here you will find an overview of account
options.

There are five tabs at the top of the account settings page:
**Profile**, **Account**, **Privacy**, **Services** and
**Applications**.

#### Profile

On this page you can change, add to or delete any of your profile
information. Go back to the end of [Part 1](1-sign_up.html) if
you want a reminder of how to edit it. Don’t forget to press the Update
button at the bottom once you’re finished!

##### “NSFW”

diaspora\* has a self-governing community standard called NSFW (not safe
for work). If you’re likely to post much material which may not be
suitable for everyone to view at their office desk (perhaps with their
boss standing behind them), please consider checking the NSFW box. This
will hide your posts from people’s streams behind a notice that the post
is marked as NSFW. They can then click this notice to view your post if
they wish.

By leaving this box unchecked, you are committing yourself not to post
any material which could potentially cause offence or get someone into
trouble for viewing it at work. If, on the other hand, you might want to
post such material only occasionally, you could leave this box unchecked
and add the \#nsfw tag to those individual posts, which will hide them
from other people’s streams.

#### Account

Here you can change your email address and password, set your language,
choose from one of our color themes, pick your email notification
preferences and download your data or close your seed. Our aim is that
you’ll be able to use the downloaded data to migrate your seed to
another pod, although this is not yet possible. You might wish to
download your data periodically as a back-up, however.

If you want, you can set your seed to automatically start sharing with
anyone who starts sharing with you. You can also re-enable the “getting
started” hints that appeared when you first opened your account.

##### Community spotlight

On the settings page you can also enable the “community spotlight,” if
this feature is available on your pod. The “community spotlight” adds
posts to your stream from community members who your pod’s admin has
selected as being people worth reading. It can be a good way to find
people to connect with when you first join diaspora\*.

#### Privacy

This is a list of users you are ignoring. You can remove them from this
list if you want to start seeing posts from them again. See [Part
5](5-sharing.html) for more on ignoring people.

#### Services

The Services page shows your connected services (e.g. Facebook, Twitter,
Tumblr) and allows you to connect new services to your diaspora\* seed.

#### Applications

This tab displays a list of any applications you have authorised in your
diaspora\* seed.

 

That’s all, folks! Thanks a lot for reading this getting started guide.
We hope it has been useful to you and that you now feel comfortable
using diaspora\* as your new online home. If there’s anything else you
want to know about any aspect of using diaspora\*, try our in-app help
section – go to your user menu in the header bar and select Help from
the drop-down menu.

If you have any questions that haven’t been answered, feel free to make
a public post on diaspora\* including the \#help and \#question tags so
that other community members can try to help you. There’s a wonderful,
generous community out there!

[Part 6 – Notifications and
conversations](6-conversations.html)
| [diaspora\* tutorials](https://diasporafoundation.org/tutorials)

#### Useful Resources

-   [Codebase](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Find & report bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - General](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Development](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Discussion -
    General](http://groups.google.com/group/diaspora-discuss)
-   [Discussion -
    Development](http://groups.google.com/group/diaspora-dev)

[![Creative Commons
License](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) is licensed
under a [Creative Commons Attribution 3.0 Unported
License](http://creativecommons.org/licenses/by/3.0/)
